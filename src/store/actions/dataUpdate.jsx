export const dataUpdate = (newData)=>
{
    console.log('dataUpdate page new data', newData)
    return(dispatch,getState,{getFirebase, getFirestore})=>{

        const firestore = getFirestore();
        firestore.collection("information").doc(newData.id).update({
            ...newData
        }).then(()=>dispatch({type: "DATA_UPDATE_PASS"}))
        .catch((err)=>dispatch({type: "DATA_UPDATE_FAIL",err}))
    }
}