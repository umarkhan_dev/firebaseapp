export const authAction = (login) =>
{
    return (dispatch, getState, {getFirebase}) =>
    {
        const firebase = getFirebase()
        firebase.auth().signInWithEmailAndPassword(
                login.email,
                login.pass
        ).then(()=>dispatch({ type: "LOGIN_PASS" }))
        .catch((err)=> dispatch({ type: "LOGIN_FAIL",err }))
    }
}


export const logoutAction = () =>
{
    return (dispatch, getState, {getFirebase}) =>
    {
        const firebase = getFirebase()
        firebase.auth().signOut()
        .then(()=>dispatch({ type: "LOGOUT_PASS" }))
        .catch((err)=> dispatch({ type: "LOGOUT_FAIL",err }))
    }
}