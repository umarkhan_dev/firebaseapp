const initState = {
show: false,
mdata: {}
}
const dataReducer = (state = initState, action) =>
{
    switch(action.type){
        case 'DATA_PASS':
        return state

        case 'DATA_FAIL':
        return state

        case 'UPDATE_SHOW':
        return {
            ...state,
            show: true
        }

        case 'HIDE_MODAL':
        return {
            ...state,
            show: false
        }
        case 'MODAL_DATA':
        console.log("modal data",action.dm)
        return {
            ...state,
            mdata: action.dm 
        }
        case 'DATA_UPDATE_PASS':
        console.log('data updated pass')
        return state

        case 'DATA_UPDATE_FAIL':
        console.log('data updated Fail',action.err)
        return state

        case 'DEL_PASS':
        console.log('dEL pass')
        return state

        case 'DEL_FAIL':
        console.log('dEL pass',action.err)
        return state

        default:
        return state;
    }
}

export default dataReducer