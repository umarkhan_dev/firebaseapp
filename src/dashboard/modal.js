import React, { Component } from 'react'
import {Modal, Button, Form} from 'react-bootstrap'
import { connect } from 'react-redux';
import  {modalAction}  from './../store/actions/modalAction';
import { dataUpdate } from './../store/actions/dataUpdate';


 class modal extends Component {
  
  constructor(props){
    super(props);
    this.state = {
      id: this.props.data.id,
      name: this.props.data.name,
      info: this.props.data.info,
      detail: this.props.data.detail
  }}
  
  handleClose = () =>
  {
    this.props.hideShow()
  }
  handleChange = (event) =>
  {
   this.setState({
     [event.target.id]: event.target.value
   })
  }

  handleUpdate =()=>
    {
      this.props.updaeData(this.state)
      this.props.hideShow()
      
    }
    
  render() {
    
    return (
      <div>
        <Modal show={this.props.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>
              <Form.Group >
               <Form.Label>Name</Form.Label>
               <Form.Control type="text" id="name" value={this.state.name} onChange={this.handleChange} />
               <Form.Text className="text-muted">
                Enter your Desired Name
               </Form.Text>
              </Form.Group>
  </Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <Form.Group controlId="formBasicEmail">
               <Form.Label>Information</Form.Label>
               <Form.Control type="text" id="info" value={this.state.info} onChange={this.handleChange} />
               <Form.Text className="text-muted">
                Enter Information
               </Form.Text>
              </Form.Group>

              <Form.Group controlId="formBasicEmail">
               <Form.Label>Detail</Form.Label>
               <Form.Control id="detail" type="text" value={this.state.detail} onChange={this.handleChange} />
               <Form.Text className="text-muted">
                Enter Extra Information
               </Form.Text>
              </Form.Group>

          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={this.handleUpdate}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal>
        
      </div>
    )
  }
}

   
const mapStateToProps = (state)=>{
return{
  show: state.dataReducer.show,
  data: state.dataReducer.mdata
}
}
const mapDispatchToProps = (dispatch)=>{
  return{
    hideShow: () => dispatch(modalAction()),
    updaeData:(newd)=> dispatch(dataUpdate(newd))
  }
  }
export default  connect(mapStateToProps,mapDispatchToProps)(modal)