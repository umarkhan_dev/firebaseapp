import React, { Component } from 'react'
import {connect} from  "react-redux"
import { authAction } from './../store/actions/authAction';
import {Redirect} from 'react-router-dom'


class siginForm extends Component {
    state = {
        email: "",
        pass: ""
    }
    handleChange = (event) =>{
        this.setState({
            [event.target.id] : event.target.value
        })
    }
    handleSubmit = (event) =>
    {
        event.preventDefault()
        this.props.login(this.state)
        

    }
    loading=()=>
    {
        return (
        <div class="spinner-grow" role="status">
        <span class="sr-only">Loading...</span>
        </div>
             )
    }
  render() {
    if(this.props.auth.uid)
    {
    return <Redirect to="/"/>
    }
    return (
      <div className="container bg-info d-flex justify-content-center">
         <div className="col-md-6 login-form-2">
                    <h3>Login</h3>
                    <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <input type="text" id="email" className="form-control" placeholder="Your Email *" onChange={this.handleChange} value={this.state.email}/>
                        </div>
                        <div className="form-group">
                            <input type="password" id="pass" className="form-control" placeholder="Your Password *" onChange={this.handleChange} value={this.state.pass }/>
                        </div>
                        <div className="form-group">
                            <input type="submit" className="btnSubmit" onClick={()=>this.loading()} value="Login" />
                        </div>
                        <div className="form-group">

                        </div>
                        <div className="d-flex justify-content-center ">
                                    <p className="badge">{this.props.auths}</p>                
                            </div>
                    </form>
                </div>
      </div>
    )
  }
}

const mapStateToProps = (state)=>
{
  return{
    auths: state.auth.authError,
    auth: state.firebase.auth
  }
}

const mapDispatchToProps = (dispatch) =>
{
    return {
        login: (creds) => dispatch(authAction(creds))
        
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(siginForm)