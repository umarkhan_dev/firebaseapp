import React from 'react'
import moment from 'moment'

export default function notification(props) {
  const {notification} = props
  return (
    <div>
      <div className="card m-4">
       
        <div className="card-body bg-warning">
       
        <ul className="navbar-nav">
           {notification && notification.map(item =>
            {
              return(
              <li key={item.id}>
              <span className="text-success">{item.content}</span>
              <span className="text-danger">{item.user}</span>
              <div className="text-light">
              {moment(item.time.toDate()).fromNow()}
              </div>
              </li>
              )
            })}
        </ul>
    
       
        </div>
        </div>
        </div>
    
  )
}
